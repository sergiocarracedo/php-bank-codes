<?php
	/**
	 * import Array.
	 *
	 * Reads a CSV file (from http://www.bde.es/f/webbde/IFI/servicio/regis/ficheros/es/REGBANESP_CONESTAB_A.XLS) and export to PHP Array file.
	 *
	 *
	 * @author Sergio Carracedo (info@sergiocarracedo.es)
	 * @license https://fsfe.org/campaigns/gplv3/gplv3.es.html
	 */

	define('CSV_FILE', 'REGBANESP_CONESTAB_A.csv');
	define('OUTPUT_FILE', 'bankcodes.php');

	$CSV_FILE = isset($argv[1]) ? $argv[1] : CSV_FILE;	

	if (!file_exists($CSV_FILE)) {
		die('File ' . $CSV_FILE .' not exits');
	}

	$fcsv = fopen($CSV_FILE, 'r');


	$row = 0;
	$res = array();
	while($csvRow = fgetcsv($fcsv)) {
		if ($row++ > 0) {
			if (!empty($csvRow[1]) && !empty($csvRow[4])) {
				$entityCode = str_pad($csvRow[1], 4, '0', STR_PAD_LEFT);
				$res[$entityCode] = $csvRow[4];
			}
		}
	}

	fclose($fcsv);

	// Output
	$foutput = fopen(OUTPUT_FILE, 'w+');
	
	fwrite($foutput, '<?php' . PHP_EOL);
	fwrite($foutput, "\t".'$bankcodes = Array(' . PHP_EOL);
	foreach($res as $entityCode => $bankname) {
		$bankname = str_replace('\'','\\\'', $bankname);
		fwrite($foutput, "\t\t'" . $entityCode . "' => '" . $bankname . "'," . PHP_EOL);
	}

	fwrite($foutput, "\t);" . PHP_EOL);
	fclose($foutput);
	print 'Output correct. Bank codes array size: ' . count($res);